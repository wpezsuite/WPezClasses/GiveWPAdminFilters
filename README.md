## WPezClasses: Give WP Admin Filters

__A collection of Admin-centric filters to enhance or expand on the free Give WordPress Donation Plugin.__

Source: https://github.com/impress-org/give-snippet-library/tree/master/admin-functions


> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example


Pseudo code-y


    $new_admin_filters = new ClassGiveWPSnippetsAdminFilters()
    
    // TODO - any setters here
    
    // When you instantiate the hooks class, pass in the filters
    $new_admin_filter_hooks = $new ClassHooks($new_admin_filters) 
    
    // If there are an filters you don't want to register, add them to an array
    $arr_exclude = []
    
    // Or you can use the updateFilters() method prior to ->register()
    
    // Pass that array in to the register method
    $new_admin_filter_hooks->register($arr_exclude);

    

    
Yup! That's it. Nice, clean, organized and ez. 


### FAQ

__1) Why?__

I see such (robust but overwhelming) libraries often and wanted to try a new pattern for packaging them into something that's ez-ier to consume. 


__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 


__3) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 



### HELPFUL LINKS

 - https://givewp.com
 
 
 - https://wordpress.org/plugins/give


 - https://github.com/impress-org/give-snippet-library/tree/master/admin-functions
 

### TODO

- Add the other Admin filters as found in the original repo's sub-folder
- Setup a separate effort for the actions


### CHANGE LOG

-v0.0.3 - Monday 22 Aril 2019
    - UPDATED: interface file / name

- v0.0.2 - 18 April 2019
    - UPDATED: namespace
    

- v0.0.1 - 12 April 2019
    - Hey! Ho!! Let's go!!! 