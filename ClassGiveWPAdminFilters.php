<?php

namespace WPezSuite\WPezClasses\GiveWPAdminFilters;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

class ClassGiveWPSnippetsAdminFilters implements InterfaceGiveWPAdminFilters {

	protected $_str_role;
	protected $_arr_imgage_size;

	public function __construct() {

		$this->setPropertyDefaults();

	}

	protected function setPropertyDefaults(){

		$this->_str_role = 'contributor';
		$this->_arr_imgage_size = false;

	}

	/**
	 *  Adds the shortcode builder button to TinyMCE editor on the admin page(s) of your choice
	 *
	 *  As of Give 2.1
	 *  ref: https://github.com/WordImpress/Give/issues/2980#issuecomment-379972047
	 */
	public function giveShortcodeButtonPages( $pages ) {

		$pages[] = 'term.php';

		return $pages;
	}


	/**
	 *  Adds the shortcode builder button to TinyMCE editor on the admin page(s) of your choice
	 *
	 *  As of Give 2.1
	 *  ref: https://github.com/WordImpress/Give/issues/2980#issuecomment-379972047
	 */
	public function getAvatar( $avatar, $id_or_email, $size, $default, $alt ) {

		$avatar = '<script src="https://www.avatarapi.com/js.aspx?email=' . $id_or_email . '&size=128"></script>';

		return $avatar;
	}



	public function setUserArgsRole( $str = 'contributor'){

		if ( is_string( $str ) ){

			$this->_str_role = $str;
			return true;

		}
		return false;
	}

	/**
	 *  Change the role that donors are assigned when they register at donation
	 *
	 *  See here for WP User Roles: https://codex.wordpress.org/Roles_and_Capabilities
	 *  See here for Give User Roles: https://givewp.com/documentation/core/give-user-roles/
	 */
	public function giveInsertUserArgs( $user_args ) {
		$user_args['role'] = $this->_str_role;
		return $user_args;
	}




	public function setImageSize( $arr = false ){

		if ( is_array( $arr )){

			$arr_def =  [
				'width'  => '400',
				'height' => '400',
				'crop'   => 1
			];

			$this->_arr_imgage_size = array_merge($arr_def, $arr);
			return true;
		}
		return false;
	}


	/**
	 *  Change the Give Single Form Featured Image sizes
	 *  @NOTE: After implementing this, be sure to run a tool like
	 *         Regenerate Thumbnails so the new image sizes can be created
	 *
	 */
	public function giveGetImageSizeGiveFormSingle( $image_size ) {

		if ( is_array($this->_arr_imgage_size)){

			return $this->_arr_imgage_size;
		}

		 return get_option( $image_size . '_image_size', array() );

	}

}