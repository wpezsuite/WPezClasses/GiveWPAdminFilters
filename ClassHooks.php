<?php

namespace WPezSuite\WPezClasses\GiveWPAdminFilters;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

class ClassHooks {

	protected $_new_component;
	protected $_arr_hook_defaults;
	protected $_arr_filters;

	public function __construct( InterfaceGiveWPAdminFilters $obj ) {

		$this->_new_component = $obj;

		$this->setPropertyDefaults();

	}

	protected function setPropertyDefaults() {

		$this->_arr_hook_defaults = [
			'active'        => true,
			'component'     => $this->_new_component,
			'priority'      => '10',
			'accepted_args' => '1'
		];

		$this->_arr_filters = [];

		$this->_arr_filters['shortcode_button_pages'] = [
			'hook'     => 'give_shortcode_button_pages',
			'callback' => 'giveShortcodeButtonPages',
		];

		$this->_arr_filters['get_avatar'] = [
			'hook'          => 'get_avatar',
			'callback'      => 'getAvatar',
			'accepted_args' => '5'
		];

		$this->_arr_filters['insert_user_args'] = [
			'hook'     => 'give_insert_user_args',
			'callback' => 'giveInsertUserArgs',
		];

		$this->_arr_filters['get_image_size_give_form_single'] = [
			'hook'     => 'give_get_image_size_give_form_single',
			'callback' => 'giveGetImageSizeGiveFormSingle',
		];

	}


	/**
	 * @param bool $arr
	 *
	 * @return bool
	 */
	public function updateHookDefaults( $arr = false ) {

		if ( is_array( $arr ) ) {

			$this->_arr_hook_defaults = array_merge( $this->_arr_hook_defaults, $arr );

			return true;

		}

		return false;
	}

	/**
	 * @return mixed
	 */
	public function getFilters() {

		return $this->_arr_filters;
	}


	/**
	 * @param bool $arr
	 *
	 * @return bool
	 */
	public function updateFilters( $arr = false ) {

		if ( is_array( $arr ) ) {

			$this->_arr_filters = array_merge( $this->_arr_filters, $arr );

			return true;

		}

		return false;
	}


	/**
	 * @param bool $arr_exclude
	 */
	public function register( $arr_exclude = false ) {

		if ( ! is_array( $arr_exclude ) ) {
			$arr_exclude = [];
		}

		foreach ( $this->_arr_filters as $str_ndx => $arr_filter ) {

			if ( in_array( $str_ndx, $arr_exclude ) ) {
				continue;
			}

			$arr = array_merge( $this->_arr_hook_defaults, $arr_filter );
			if ( $arr['active'] === false ) {
				continue;
			}

			add_filters(
				$arr['hook'],
				[ $arr['component'], $arr['calback'] ],
				$arr['priority'],
				$arr['accepted_args']
			);
		}

	}

}