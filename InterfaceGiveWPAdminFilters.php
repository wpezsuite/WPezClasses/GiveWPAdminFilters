<?php

namespace WPezSuite\WPezClasses\GiveWPAdminFilters;

interface InterfaceHooks {

	public function giveShortcodeButtonPages( $pages );

	public function getAvatar( $avatar, $id_or_email, $size, $default, $alt );

	public function giveInsertUserArgs( $user_args );

	public function giveGetImageSizeGiveFormSingle( $image_size );

}